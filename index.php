<?php

$txtnvuelo=(isset($_POST['txtnvuelo']))?$_POST['txtnvuelo']:"";
$txtOrigen=(isset($_POST['txtOrigen']))?$_POST['txtOrigen']:"";
$Destino=(isset($_POST['Destino']))?$_POST['Destino']:"";
$txtFechsal=(isset($_POST['txtFechsal']))?$_POST['txtFechsal']:"";
$txtHora=(isset($_POST['txtHora']))?$_POST['txtHora']:"";
$txtTipoa=(isset($_POST['txtTipoa']))?$_POST['txtTipoa']:"";


$accion=(isset($_POST['accion']))?$_POST['accion']:"";

$error=array();

$accionAgregar=$accionCancelar="";
$accionModificar="disabled";
$mostrarModal=false;

include ("conexion/conexion.php");

switch($accion){
    case "btnAgregar":
        
        if($txtOrigen==""){
            $error['origen']="Escribe el origen del Empleado";
        }
        if($Destino==""){
            $error['destino']="Escribe los destino del Empleado";
        }
        if($txtFechsal==""){
            $error['fech_salida']="Escribe la fech_salida del Empleado";
        }
        if($txtHora==""){
            $error['hora_salida']="Escribe la Fecha de Nacimiento del Empleado";
        }
        if($txtTipoa==""){
            $error['tipo_avion']="Escribe el tipo_avion del Empleado";
        }
        if(count($error)>0){
            $mostrarModal=true;
            break;
        }
        
        $sentencia=$pdo->prepare("INSERT INTO vuelos(origen,destino,fech_salida,hora_salida,tipo_avion)
        VALUES (:origen,:destino,:fech_salida,:hora_salida,:tipo_avion) ");
        
        $sentencia->bindParam(':origen',$txtOrigen);
        $sentencia->bindParam(':destino',$Destino);
        $sentencia->bindParam(':fech_salida',$txtFechsal);
        $sentencia->bindParam(':hora_salida',$txtHora);
        $sentencia->bindParam(':tipo_avion',$txtTipoa);
        $sentencia->execute();
            
        header('Location: index.php');    
        
    break;
    
    case "btnModificar":
        
    
        $sentencia=$pdo->prepare("UPDATE vuelos SET
        origen=:origen,
        destino=:destino,
        fech_salida=:fech_salida,
        hora_salida=:hora_salida,
        tipo_avion=:tipo_avion  WHERE num_vuelo=:num_vuelo");
        
        $sentencia->bindParam(':num_vuelo',$txtnvuelo);
        $sentencia->bindParam(':origen',$txtOrigen);
        $sentencia->bindParam(':destino',$Destino);
        $sentencia->bindParam(':fech_salida',$txtFechsal);
        $sentencia->bindParam(':hora_salida',$txtHora);
        $sentencia->bindParam(':tipo_avion',$txtTipoa);
        $sentencia->execute();
        
        header('Location: index.php');
        
        
       
    break;
    case "btnEliminar":
        
        $sentencia=$pdo->prepare("SELECT * FROM vuelos WHERE num_vuelo=:num_vuelo");
        $sentencia->bindParam(':num_vuelo',$txtnvuelo);
        $sentencia->execute();
        $empleado=$sentencia->fetch(PDO::FETCH_LAZY);
        print_r($empleado);

        
        $sentencia=$pdo->prepare(" DELETE FROM vuelos WHERE num_vuelo=:num_vuelo");
        $sentencia->bindParam(':num_vuelo',$txtnvuelo);
        $sentencia->execute();
        header('Location: index.php');
        
        echo $txtnvuelo;
        echo "Presionaste btnEliminar";
   
        
    break;
    case "btnCancelar":
        header('Location: index.php');
   
        
    break;
    
    case "Editar":
        $accionAgregar="disabled";
        $accionModificar=$accionEliminar=$accionCancelar="";
        $mostrarModal=true;
        
        $sentencia=$pdo->prepare("SELECT * FROM vuelos WHERE num_vuelo=:num_vuelo");
        $sentencia->bindParam(':num_vuelo',$txtnvuelo);
        $sentencia->execute();
        $empleado=$sentencia->fetch(PDO::FETCH_LAZY);
        
        
        $txtOrigen=$empleado['origen'];
        $Destino=$empleado['destino'];
        $txtFechsal=$empleado['fech_salida'];
        $txtHora=$empleado['hora_salida'];
        $txtTipoa=$empleado['tipo_avion'];
        
        
        
        
    break;
}

    $sentencia=$pdo->prepare("SELECT * FROM `vuelos` WHERE 1");
    $sentencia->execute();
    $listaEmpleados=$sentencia->fetchALL(PDO::FETCH_ASSOC);

   
?>

<!DOCTYPE html>
<html lang="es-MX">
<head>
    <meta http-equiv="content-type" content="text/html" charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registro de Vuelos</title>
    <!-- Bootstrap y JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js"></script>
    <!-- Fuentes -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,900&display=swap" rel="stylesheet">
       
</head>
<body>
<nav class="navbar navbar-expand-sm bg-primary navbar-dark">
  <ul class="navbar-nav">
    <li class="nav-item ">
      <a class="nav-link" href="index.php">Registro de Vuelos</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="reservacion.php">Reservaciones</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Link</a>
    </li>
  </ul>
</nav>
<br>
       <div class="container">
        <form  action="" method="post" enctype="multipart/form-data">
                

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="text-dark" class="modal-title" id="exampleModalLabel">Empleado</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="text-dark" class="modal-body">
        <div>
            <label for="">Numero de vuelo:</label>
            <input type="text" class="form-control <?php echo (isset($error['num_vuelo']))?"is-invalid":"";?>" name="txtnvuelo" value="<?php echo $txtnvuelo;?>" placeholder="El num_vuelo se agrega automaticamente" id="txt1" readonly>
            <div class="invalid-feedback"> 
            <?php echo (isset($error['num_vuelo']))?$error['num_vuelo']:"";?>
            </div>
    
            <br>
            
            <label for="">Origen:</label>
            <input type="text" class="form-control <?php echo (isset($error['origen']))?"is-invalid":"";?>" name="txtOrigen" value="<?php echo $txtOrigen;?>" placeholder="" id="txt2" >
            <div class="invalid-feedback"> 
            <?php echo (isset($error['origen']))?$error['origen']:"";?>
            </div>
            <br>
            
            <label for="">Destino:</label>
            <input type="text" class="form-control <?php echo (isset($error['destino']))?"is-invalid":"";?> " name="Destino" value="<?php echo $Destino;?>" placeholder="" id="txt3" >
            <div class="invalid-feedback"> 
            <?php echo (isset($error['destino']))?$error['destino']:"";?>
            </div>
            <br>
            
            <label for="">Fecha de salida:</label>
            <input type="date" class="form-control <?php echo (isset($error['fech_salida']))?"is-invalid":"";?>" name="txtFechsal" value="<?php echo $txtFechsal;?>" placeholder="" id="txt7" >
            <div class="invalid-feedback"> 
            <?php echo (isset($error['fech_salida']))?$error['fech_salida']:"";?>
            </div>
            <br>


            <label for="">Hora de salida:</label>
            <input type="text" class="form-control <?php echo (isset($error['hora_salida']))?"is-invalid":"";?>" name="txtHora" value="<?php echo $txtHora;?>" placeholder="" id="txt9" >
            <div class="invalid-feedback"> 
            <?php echo (isset($error['hora_salida']))?$error['hora_salida']:"";?>
            </div>
            <br>

            <label for="">Tipo de avion:</label>
            <input type="text" class="form-control <?php echo (isset($error['tipo_avion']))?"is-invalid":"";?>" name="txtTipoa" value="<?php echo $txtTipoa;?>" placeholder="" id="txt4" >
            <div class="invalid-feedback"> 
            <?php echo (isset($error['tipo_avion']))?$error['tipo_avion']:"";?>
            </div>
            <br>
            
          </div>
      </div>
      <div class="modal-footer">
            <button value="btnAgregar" <?php echo $accionAgregar;?> class="btn btn-success" type="submit" name="accion" >Agregar registro</button>
            <button value="btnModificar"  <?php echo $accionModificar;?> class="btn btn-warning" type="submit" name="accion" >Actualizar</button>
            <button value="btnCancelar" <?php echo $accionCancelar;?> class="btn btn-danger" type="submit" name="accion" data-dismiss="modal">Cancelar</button>
            
      </div>
    </div>
  </div>
</div>

            <!-- Button trigger modal -->
<button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#exampleModal">
  Nuevo registro de vuelo
</button>
            
            
        
     
        </form>
        <br>
        <div class="row">
        
            <table class="table table-hover table-dark table-bordered text-center">
                
                
                <thead lass="thead-dark">
                    <tr>
                        <th>Numero de vuelo</th>
                        <th>Origen</th>
                        <th>Destino</th>
                        <th>Fecha de salida</th>
                        <th>Hora de salida</th>
                        <th>Tipo de avion</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
            <?php foreach($listaEmpleados as $empleado){ ?>   
                
                <tr>
                        <td><?php echo $empleado['num_vuelo']; ?></td>
                        <td><?php echo $empleado['origen']; ?></td>
                        <td><?php echo $empleado['destino']; ?></td>
                        <td><?php echo $empleado['fech_salida']; ?></td>
                        <td><?php echo $empleado['hora_salida']; ?></td>
                        <td><?php echo $empleado['tipo_avion']; ?></td>
                        

                        <td>
                            
                            <form action="" method="post">
                                
                                <input type="hidden" name="txtnvuelo" value="<?php echo $empleado['num_vuelo']; ?>">
                                
                                
                                
                            <input type="submit" value="Editar" class="btn btn-warning" name="accion">
                            <button value="btnEliminar" type="submit" class="btn btn-danger" name="accion">Eliminar</button>
                                
                            </form>
                            
                             
                        </td>
                    </tr>
                
            <?php } ?>    
            </table>
        
        </div>
        <?php if($mostrarModal){?>
            <script>
                $('#exampleModal').modal('show');
            </script>
        <?php }?>
    </div>
    

    
</body>
</html>