<?php

$txtidpas=(isset($_POST['txtidpas']))?$_POST['txtidpas']:"";
$txtnombre=(isset($_POST['txtnombre']))?$_POST['txtnombre']:"";
$txtHora=(isset($_POST['txtHora']))?$_POST['txtHora']:"";
$txtFechsal=(isset($_POST['txtFechsal']))?$_POST['txtFechsal']:"";
$Destino=(isset($_POST['Destino']))?$_POST['Destino']:"";



$accion=(isset($_POST['accion']))?$_POST['accion']:"";

$error=array();

$accionAgregar=$accionCancelar="";
$accionModificar="disabled";
$mostrarModal=false;

include ("conexion/conexion.php");

switch($accion){
    case "btnAgregar":
        
        if($txtnombre==""){
            $error['id_pasajero']="Escribe el id del pasajero";
        }
        if($txtnombre==""){
            $error['nombre']="Escribe el nombre del pasajero";
        }
        if($txtHora==""){
            $error['hora_salida']="Escribe la hora de salida del pasajero";
        }
        if($txtFechsal==""){
            $error['fecha_salida']="Escribe la fecha_salida del pasajero";
        }
        if($Destino==""){
            $error['destino']="Escribe los destino del pasajero";
        }
        if(count($error)>0){
            $mostrarModal=true;
            break;
        }
        
        $sentencia=$pdo->prepare("INSERT INTO reservacion(nombre,hora_salida,fecha_salida,destino)
        VALUES (:nombre,:hora_salida,:fecha_salida,:destino) ");
        
        $sentencia->bindParam(':nombre',$txtnombre);
        $sentencia->bindParam(':hora_salida',$txtHora);
        $sentencia->bindParam(':fecha_salida',$txtFechsal);
        $sentencia->bindParam(':destino',$Destino);
        $sentencia->execute();
            
        header('Location: reservacion.php');    
        
    break;
    
    case "btnModificar":
        
    
        $sentencia=$pdo->prepare("UPDATE reservacion SET
        nombre=:nombre,
        hora_salida=:hora_salida,
        fecha_salida=:fecha_salida,
        destino=:destino  WHERE id_pasajero=:id_pasajero");
        
        $sentencia->bindParam(':id_pasajero',$txtidpas);
        $sentencia->bindParam(':nombre',$txtnombre);
        $sentencia->bindParam(':hora_salida',$txtHora);
        $sentencia->bindParam(':fecha_salida',$txtFechsal);
        $sentencia->bindParam(':destino',$Destino);
        $sentencia->execute();
        
        header('Location: reservacion.php');
        
        
       
    break;
    case "btnEliminar":
        
        $sentencia=$pdo->prepare("SELECT * FROM reservacion WHERE id_pasajero=:id_pasajero");
        $sentencia->bindParam(':id_pasajero',$txtidpas);
        $sentencia->execute();
        $empleado=$sentencia->fetch(PDO::FETCH_LAZY);
        print_r($empleado);

        
        $sentencia=$pdo->prepare(" DELETE FROM reservacion WHERE id_pasajero=:id_pasajero");
        $sentencia->bindParam(':id_pasajero',$txtidpas);
        $sentencia->execute();
        header('Location: reservacion.php');
        
        echo $txtidpas;
        echo "Presionaste btnEliminar";
   
        
    break;
    case "btnCancelar":
        header('Location: reservacion.php');
   
        
    break;
    
    case "Editar":
        $accionAgregar="disabled";
        $accionModificar=$accionEliminar=$accionCancelar="";
        $mostrarModal=true;
        
        $sentencia=$pdo->prepare("SELECT * FROM reservacion WHERE id_pasajero=:id_pasajero");
        $sentencia->bindParam(':id_pasajero',$txtidpas);
        $sentencia->execute();
        $empleado=$sentencia->fetch(PDO::FETCH_LAZY);
        
        
        $txtnombre=$empleado['nombre'];
        $txtHora=$empleado['hora_salida'];
        $txtFechsal=$empleado['fecha_salida'];
        $Destino=$empleado['destino'];
        
        
        
        
    break;
}

    $sentencia=$pdo->prepare("SELECT * FROM `reservacion` WHERE 1");
    $sentencia->execute();
    $listaEmpleados=$sentencia->fetchALL(PDO::FETCH_ASSOC);

   
?>

<!DOCTYPE html>
<html lang="es-MX">
<head>
    <meta http-equiv="content-type" content="text/html" charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reservaciones</title>
    <!-- Bootstrap y JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js"></script>
    <!-- Fuentes -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,900&display=swap" rel="stylesheet">
       
</head>
<body>
<nav class="navbar navbar-expand-sm bg-primary navbar-dark">
  <ul class="navbar-nav">
    <li class="nav-item ">
      <a class="nav-link" href="index.php">Registro de Vuelos</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="reservacion.php">Reservaciones</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Link</a>
    </li>
  </ul>
</nav>
<br>
       <div class="container">
        <form  action="" method="post" enctype="multipart/form-data">
                

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="text-dark" class="modal-title" id="exampleModalLabel">Empleado</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="text-dark" class="modal-body">
        <div>
            <label for="">Id del pasajero:</label>
            <input type="text" class="form-control <?php echo (isset($error['id_pasajero']))?"is-invalid":"";?>" name="txtidpas" value="<?php echo $txtidpas;?>" placeholder="Se agrega automaticamente" id="txt1" readonly >
            <div class="invalid-feedback"> 
            <?php echo (isset($error['id_pasajero']))?$error['id_pasajero']:"";?>
            </div>
    
            <br>
            
            <label for="">Nombre:</label>
            <input type="text" class="form-control <?php echo (isset($error['nombre']))?"is-invalid":"";?>" name="txtnombre" value="<?php echo $txtnombre;?>" placeholder="" id="txt2" >
            <div class="invalid-feedback"> 
            <?php echo (isset($error['nombre']))?$error['nombre']:"";?>
            </div>
            <br>
            
            <label for="">Hora de salida:</label>
            <input type="text" class="form-control <?php echo (isset($error['hora_salida']))?"is-invalid":"";?>" name="txtHora" value="<?php echo $txtHora;?>" placeholder="" id="txt9" >
            <div class="invalid-feedback"> 
            <?php echo (isset($error['hora_salida']))?$error['hora_salida']:"";?>
            </div>
            <br>

            <label for="">Fecha de salida:</label>
            <input type="date" class="form-control <?php echo (isset($error['fecha_salida']))?"is-invalid":"";?>" name="txtFechsal" value="<?php echo $txtFechsal;?>" placeholder="" id="txt7" >
            <div class="invalid-feedback"> 
            <?php echo (isset($error['fecha_salida']))?$error['fecha_salida']:"";?>
            </div>
            <br>

            <label for="">Destino:</label>
            <input type="text" class="form-control <?php echo (isset($error['destino']))?"is-invalid":"";?> " name="Destino" value="<?php echo $Destino;?>" placeholder="" id="txt3" >
            <div class="invalid-feedback"> 
            <?php echo (isset($error['destino']))?$error['destino']:"";?>
            </div>
            <br>
            
            
          </div>
      </div>
      <div class="modal-footer">
            <button value="btnAgregar" <?php echo $accionAgregar;?> class="btn btn-success" type="submit" name="accion" >Agregar registro</button>
            <button value="btnModificar"  <?php echo $accionModificar;?> class="btn btn-warning" type="submit" name="accion" >Actualizar</button>
            <button value="btnCancelar" <?php echo $accionCancelar;?> class="btn btn-danger" type="submit" name="accion" data-dismiss="modal">Cancelar</button>
            
      </div>
    </div>
  </div>
</div>

            <!-- Button trigger modal -->
<button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#exampleModal">
  Nuevo registro de reservacion
</button>
            
            
        
     
        </form>
        <br>
        <div class="row">
        
            <table class="table table-hover table-dark table-bordered text-center">
                
                
                <thead lass="thead-dark">
                    <tr>
                        <th>Id del pasajero</th>
                        <th>Nombre</th>
                        <th>Hora de salida</th>
                        <th>Fecha de salida</th>
                        <th>Destino</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
            <?php foreach($listaEmpleados as $empleado){ ?>   
                
                <tr>
                        <td><?php echo $empleado['id_pasajero']; ?></td>
                        <td><?php echo $empleado['nombre']; ?></td>
                        <td><?php echo $empleado['hora_salida']; ?></td>
                        <td><?php echo $empleado['fecha_salida']; ?></td>
                        <td><?php echo $empleado['destino']; ?></td>
                        

                        <td>
                            
                            <form action="" method="post">
                                
                                <input type="hidden" name="txtidpas" value="<?php echo $empleado['id_pasajero']; ?>">
                                
                                
                                
                            <input type="submit" value="Editar" class="btn btn-warning" name="accion">
                            <button value="btnEliminar" type="submit" class="btn btn-danger" name="accion">Eliminar</button>
                                
                            </form>
                            
                             
                        </td>
                    </tr>
                
            <?php } ?>    
            </table>
        
        </div>
        <?php if($mostrarModal){?>
            <script>
                $('#exampleModal').modal('show');
            </script>
        <?php }?>
    </div>
    

    
</body>
</html>